import ctypes
import numpy
import threading
import logging
import warnings
import copy
from math import ceil
from ctypes.wintypes import DWORD, HANDLE, WORD
# noinspection PyPackageRequirements
from . import pcoError
from . import ImgBuf
from .version import __version__

# Setup some type definitions
cInt = ctypes.c_int
int32 = ctypes.c_long
uInt32 = ctypes.c_ulong
uInt64 = ctypes.c_ulonglong
float64 = ctypes.c_double
# load DLLs
sensicamdll = ctypes.cdll.sen_cam
# create logger
logger = logging.getLogger(__name__)


class DPTimeLimitsDescription(ctypes.Structure):
    """ Class for C struct DP_TimeLimits_Description (SC_SDKStructures.h)
    """
    DP_DESC_STEP_NUM = 100  # SC_SDKStructures.h l.159
    _fields_ = [('wSize', WORD),
                ('wCameraSubmode', WORD),
                ('dwDelaymin', DWORD),
                ('dwDelaymax', DWORD),
                ('dwExposmin', DWORD),
                ('dwExposmax', DWORD),
                ('wDelayTab', WORD * DP_DESC_STEP_NUM),
                ('wExposTab', WORD * DP_DESC_STEP_NUM),
                ('wDelayStep100', WORD),
                ('wDelayStep1000', WORD),
                ('wExposStep100', WORD),
                ('wExposStep1000', WORD)
                ]

    def __init__(self):
        super(DPTimeLimitsDescription, self).__init__()
        self.wSize = ctypes.sizeof(self)


class DicamProDesc(ctypes.Structure):
    """ Class for C struct DP_Control_Description (SC_SDKStructures.h)
    """
    DP_DESC_LIMITS_NUM = 4  # SC_SDKStructures.h l.160
    _fields_ = [('wSize', WORD),
                ('wPulserType', WORD),
                ('wPulseInterval', WORD),
                ('wPhosphordecaymin', WORD),
                ('wPhosphordecaymax', WORD),
                ('strTimeLimits', DPTimeLimitsDescription * DP_DESC_LIMITS_NUM),
                ('dwReserved', DWORD * 16)]

    def __init__(self):
        super(DicamProDesc, self).__init__()
        self.wSize = ctypes.sizeof(self)


class SC_TimeLimits_Description(ctypes.Structure):
    _fields_ = [('wSize', WORD),
                ('wCameraSubmode', WORD),
                ('dwMinDelayDESC', DWORD),
                ('dwMaxDelayDESC', DWORD),
                ('dwMinDelayStepDESC', DWORD),
                ('dwMinExposureDESC', DWORD),
                ('dwMaxExposureDESC', DWORD),
                ('dwMinExposureStepDESC', DWORD)
                ]

    def __init__(self):
        super(SC_TimeLimits_Description, self).__init__()
        self.wSize = ctypes.sizeof(self)


class SC_Camera_Description(ctypes.Structure):
    SC_DESC_LIMITS_NUM = 16
    SC_DESC_VERTTAB_NUM = 16
    SC_DESC_HORZTAB_NUM = 4
    _fields_ = [('wSize', WORD),
                ('wCameraTypeDESC', WORD),
                ('wSensorTypeDESC', WORD),
                ('wCameraMode', WORD),
                ('wMaxHorzResDESC', WORD),
                ('wMaxVertResDESC', WORD),
                ('wRoiHorStepsDESC', WORD),
                ('wRoiVertStepsDESC', WORD),
                ('wBinHorzTab', WORD * SC_DESC_HORZTAB_NUM),
                ('wBinVertTab', WORD * SC_DESC_VERTTAB_NUM),
                ('wDynResDESC', WORD),
                ('wNumADCsDESC', WORD),
                ('wColorPatternDESC', WORD),
                ('wPatternTypeDESC', WORD),
                ('strTimeLimits', SC_TimeLimits_Description * SC_DESC_LIMITS_NUM),
                # SC_TimeLimits_Description strTimeLimits[SC_DESC_LIMITS_NUM] TODO
                ('wIRDESC', WORD),
                ('wDoubleImageDESC', WORD),
                ('wPowerDownModeDESC', WORD),
                ('wDummy', WORD),
                ('dwPowerdownMinWakeup', WORD),
                ('dwPowerdownMinTime', WORD),
                ('wOffsetRegulationDESC', WORD),
                ('wFPGAVersion', WORD),
                ('dwGeneralCapsDESC1', DWORD),
                ('dwGeneralCapsDESC2', DWORD),
                ('dwReservedDESC', DWORD * 20),
                ]

    def __init__(self):
        super(SC_Camera_Description, self).__init__()
        self.wSize = ctypes.sizeof(self)


class CamParam(ctypes.Structure):
    """ Class for C struct cam_param
    """
    _fields_ = [('hdriver', HANDLE),
                ('boardnr', cInt),
                ('boardtyp', cInt),
                ('boardrev', cInt),
                ('cam_typ', cInt),
                ('cam_ccd', cInt),
                ('cam_id', cInt),
                ('ccdwidth', cInt),
                ('ccdheight', cInt),
                ('bit_pix', cInt)]


class CamValues(ctypes.Structure):
    """ Class for C struct cam_values
    """
    _fields_ = [('runcoc', cInt),
                ('pic_in_buf', cInt),
                ('ccdtemp', cInt),
                ('eletemp', cInt),
                ('readtime', cInt),
                ('coctime', ctypes.c_float),
                ('beltime', ctypes.c_float),
                ('exptime', ctypes.c_float),
                ('deltime', ctypes.c_float)]


class SensicamError(Exception):
    """
    Sensicam expection handle
    """

    def __init__(self, err):
        self.errStr = pcoError.getText(err)
        self.logger = logging.getLogger(__name__)
        self.logger.error(self.errStr)

    def __str__(self):
        return self.errStr


class Sensicam(threading.Thread):
    """Python wrapper for sensicam SDK from pco.

    This class is a Python wrapper for the Sensicam SDK of pco AG, Germany
    and provides an API to handle an pco camera in python.
    It is tested and used with a Dicam Pro.
    The constructor automatically intialized the PCI board and allocates
    hdriver accordingly.

    :requires: Sensicam SDK (>=v6.01.04), pco.camware(>=v3.16)

    :param board: number of PCI-Controller-Board. Default: 0
    :type board: int or ctypes.c_int
    :param: dicamProDesc
    :type: class DicamProDesc
    :param hdriver: Filehandle of the opened driver. Default: None
    :type hdriver: ctypes.wintypes.HANDLE
    :param ccdxsize: resolution of CCD in x
    :type ccdxsize: ctypes.c_int
    :param ccdysize: resolution of CCD in y
    :type ccdysize: ctypes.c_int
    :param curWidth: current width of image
    :type curWidth: ctypes.c_int
    :param curHeight: current height of image
    :type curHeight: ctypes.c_int
    :param curBitpix: current bits per pixel in image
    :type curBitpix: ctypes.c_int

    Example::

        import dicamsdk
        import logging
        import matplotlib.pyplot as plt

        # Create logger
        logging.basicConfig(level=logging.DEBUG, filename='example.log',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        board = 0
        with dicamsdk.Sensicam(board) as pcoCam:
            # Setup a the COC
            gain = 0  # normal analog gain
            submode = 0  # sequential, busy out
            roixmin = 1
            roixmax = 1
            roiymin = 1
            roiymax = 1
            hbin = 1
            vbin = 1
            # Table values:
            phosphordecay, mcpgain, trigger, loops = 1, 999, 0, 1
            imageTime1 = [0, 1000, 0, 300]
            table = pcoCam.dicamTableGen(phosphordecay, mcpgain, trigger, loops,
                                         imageTime1)
            camera_type = 5  # For dicam pro
            pcoCam.setCoc(gain, camera_type, submode, roixmin, roixmax, roiymin,
                          roiymax, hbin, vbin, table)
            print("COC loaded")
            # Get sizes and allocate buffer
            pcoCam.getSizes()
            # Initialize buffers
            buf1 = dicamsdk.ImgBuf()
            buf2 = dicamsdk.ImgBuf()
            pcoCam.allocateBuffer(buf1)
            pcoCam.mapBuffer(buf1)
            pcoCam.setBufferEvent(buf1)
            pcoCam.allocateBuffer(buf2)
            pcoCam.mapBuffer(buf2)
            pcoCam.setBufferEvent(buf2)
            # Clear the board buffer with stopCoc command
            pcoCam.stopCoc()
            print('status buf1:', buf1.getStatus())
            # Set camera to state RUN
            pcoCam.runCoc()
            pcoCam.addBufferToList(buf1)
            pcoCam.addBufferToList(buf2)
            print('status buf1:', buf1.getStatus())
            pcoCam.stopCoc()
            npImage1 = pcoCam.getImageFromBuffer(buf1)
            npImage2 = pcoCam.getImageFromBuffer(buf2)
            # Free all buffers and close the camera
            pcoCam.removeAllBuffersFromList()
            pcoCam.freeBuffer(buf1)
            pcoCam.freeBuffer(buf2)
            # Show the image
            plt.ion()
            plt.imshow(npImage1)
        print("Card closed.")

    """

    def __init__(self, board=None, hdriver=None):
        """
        Constructor
        """
        threading.Thread.__init__(self)
        self.logger = logging.getLogger(__name__)
        if int is type(board):
            self.board = cInt(board)
        elif cInt is type(board):
            self.board = board
        elif not board:
            self.board = cInt(0)
        else:
            raise TypeError('board must be int or ctypes.c_long')
        if not hdriver:
            self.hdriver = HANDLE(None)
        elif HANDLE != type(hdriver):
            raise TypeError("hdriver must be of type HANDLE")
        else:
            self.hdriver = hdriver
        # Initialize the board
        sensicamdll.INITBOARD.argtypes = (cInt, ctypes.POINTER(HANDLE))
        sensicamdll.INITBOARD.restype = cInt
        self.errCheck(sensicamdll.INITBOARD(self.board,
                                            ctypes.byref(self.hdriver)))
        self.ccdxsize = cInt(-1)
        self.ccdysize = cInt(-1)
        self.curWidth = cInt(-1)
        self.curHeight = cInt(-1)
        self.curBitpix = cInt(-1)
        self.mode = cInt(-1)
        self.camera_type = cInt(-1)
        self.gain = cInt(-1)
        self.submode = cInt(-1)
        self.trig = cInt(-1)
        self.roixmin = cInt(-1)
        self.roixmax = cInt(-1)
        self.roiymin = cInt(-1)
        self.roiymax = cInt(-1)
        self.hbin = cInt(-1)
        self.vbin = cInt(-1)
        self.table = None
        self.dicamProDesc = None
        self.SC_Camera_Description = None
        self.__version__ = __version__
        self.logger.debug("Board successfull initialized")

    # noinspection PyMethodMayBeStatic
    def errCheck(self, err):
        """Returns the error number from pcoError

        Takes the provided error number and uses the function getText(err)
        from module pcoError to retrieve the error message.
        If the error code is nor an error neither a warning, the function
        raises a RuntimeError.

        :param err: Number with the error of the another method
        :type: int
        """
        if int != type(err):
            raise TypeError('err must be int')
        if 0 != err:
            err = DWORD(err)
            errHex = hex(err.value)
            # error start with 0x08
            # warning starts with 0xc0
            if errHex.startswith('0xc0'):
                warnStr = pcoError.getText(err.value)
                warnings.warn(warnStr)
                self.logger.warning(warnStr)
            else:
                raise SensicamError(err.value)

    def close(self):
        """Closes the connection to the board.
        """
        sensicamdll.CLOSEBOARD.argtype = ctypes.POINTER(HANDLE)
        sensicamdll.CLOSEBOARD.restype = cInt
        self.errCheck(sensicamdll.CLOSEBOARD(ctypes.byref(self.hdriver)))
        self.logger.debug("Board successfully closed")

    def getDicamProDesc(self):
        """Get the description of the current connected DICAM PRO. Values are written to camera object as well.

        :rtype: class DicamProDesc
        :return: dicamProDesc
        """
        sensicamdll.GET_DICAMPRO_DESC.argtypes = (HANDLE,
                                                  ctypes.POINTER(DicamProDesc))
        sensicamdll.GET_DICAMPRO_DESC.restype = cInt
        self.dicamProDesc = DicamProDesc()
        self.errCheck(sensicamdll.GET_DICAMPRO_DESC(self.hdriver, self.dicamProDesc))
        self.logger.debug("dicamProDesc aquired")
        return self.dicamProDesc

    def getCameraDesc(self):
        """Get the description of the current connected camera. Values are written to camera object as well.

        :rtype: class SC_Camera_Description
        :return: SC_Camera_Description
        """
        sensicamdll.GET_DICAMPRO_DESC.argtypes = (HANDLE,
                                                  ctypes.POINTER(DicamProDesc))
        sensicamdll.GET_DICAMPRO_DESC.restype = cInt
        self.SC_Camera_Description = SC_Camera_Description()
        self.errCheck(sensicamdll.GET_CAMERA_DESC(self.hdriver, self.SC_Camera_Description))
        self.logger.debug("SC_Camera_Description aquired")
        return self.SC_Camera_Description

    def setup_camera(self):
        """Sets up the camera
        """
        sensicamdll.SETUP_CAMERA.argtype = HANDLE
        sensicamdll.SETUP_CAMERA.restype = cInt
        self.errCheck(sensicamdll.SETUP_CAMERA(self.hdriver))
        self.logger.debug("Camera successfully set-up")

    def setCoc(self, gain, camera_type, submode, roixmin, roixmax, roiymin,
               roiymax, hbin, vbin, table, trig=None):
        """The Camera Operation Code (COC) is set with this function.

        Further details can be obtained in the official SDK description.

        :param gain: chosen gain (0 normal analog, 1 extended analog)
        :type gain: int or ctypes.c_int
        :param camera_type: Selects the used camera (e.g. 5 for Dicam Pro)
        :type camera_type: int or ctypes.c_int
        :param submode: Choses the submode (e.g. 0)
        :type submode: int or ctypes.c_int
        :param roixmin: Region of interest min value in x
        :type roixmin: int or ctypes.c_int
        :param roixmax: Region of interest max value in x
        :type roixmax: int or ctypes.c_int
        :param roiymin: Region of interest min value in y
        :type roiymin: int or ctypes.c_int
        :param roiymax: Region of interest max value in y
        :type roiymax: int or ctypes.c_int
        :param hbin: Value for horizontal binning
        :type hbin: int or ctypes.c_int
        :param vbin: Value for vertical binning
        :type vbin: int or ctypes.c_int
        :param table: Set the delay and exposure times. Time values are
            separated by comma. The array concludes with the sequence
            "-1,-1". Detailed information in SDK manual
        :type table: str
        :param trig: Value for trigger. Default 0
        :type trig: int or ctypes.c_int
        """
        if trig is None:
            trig = cInt(0)
        # Check for valid max roi values (issue #42)
        roixmaxcam = int(ceil(self.ccdxsize.value / 32))
        roiymaxcam = int(ceil(self.ccdysize.value / 32))
        if roixmax > roixmaxcam:
            self.logger.warning('roixmax is out of range. Has been set to maximal value of {}.'.format(roixmaxcam))
            warnings.warn('roixmax is out of range. Has been set to maximal value of {}.'.format(roixmaxcam))
            roixmax = roixmaxcam
        if roiymax > roiymaxcam:
            self.logger.warning('roiymax is out of range. Has been set to maximal value of {}.'.format(roiymaxcam))
            warnings.warn('roiymax is out of range. Has been set to maximal value of {}.'.format(roiymaxcam))
            roiymax = roiymaxcam
        inputCoc = [gain, camera_type, submode, roixmin, roixmax, roiymin, roiymax, hbin, vbin, table]
        # Adjust the input with testCoc()
        newCoc = self.testCoc(gain, camera_type, submode, roixmin, roixmax, roiymin,
                              roiymax, hbin, vbin, table, trig)
        if newCoc:
            # Format values from string buffer. For tables with several lines the try part is used, for simple sensicam
            # the except part
            try:
                tmpTable = str(newCoc[9].value).split(r'\r\n')[:-1]
                completeTable = (tmpTable[0][2:] + tmpTable[1] + tmpTable[2])
            except IndexError:
                completeTable = newCoc[9].value.decode("ASCII")[:-2]
            props = ['gain', 'camera_type', 'submode', 'roixmin', 'roixmax', 'roiymin', 'roiymax', 'hbin', 'vbin']
            for ii in range(len(props)):
                if inputCoc[ii] != newCoc[ii]:
                    self.logger.warning(
                        'Input for {} has been changed from {} to {}.'.format(props[ii], inputCoc[ii], newCoc[ii]))
                    warnings.warn(
                        'Input for {} has been changed from {} to {}.'.format(props[ii], inputCoc[ii], newCoc[ii]))
            if completeTable != table:
                self.logger.warning('Input table has been changed.')
                warnings.warn('Input table has been changed.')
            [gain, camera_type, submode, roixmin, roixmax, roiymin,
             roiymax, hbin, vbin, table] = newCoc
        # Convert the input
        self.camera_type = camera_type
        self.gain = gain
        self.submode = submode
        if (int is type(camera_type)) and (int is type(camera_type)):
            self.mode = cInt(self.camera_type +
                             (self.gain * 256) + (self.submode * 65536))  # p.24
        else:
            self.mode = cInt(self.camera_type.value +
                             (self.gain.value * 256) + (self.submode.value * 65536))  # p.24
        if int is type(trig):
            trig = cInt(trig)
        self.trig = trig
        self.roixmin = roixmin
        self.roixmax = roixmax
        self.roiymin = roiymin
        self.roiymax = roiymax
        self.hbin = hbin
        self.vbin = vbin
        if str is type(table):
            self.table = ctypes.create_string_buffer(table.encode('utf-8'), 200)
        else:
            self.table = table

        sensicamdll.SET_COC.argtypes = (HANDLE,
                                        cInt,
                                        cInt,
                                        cInt,
                                        cInt,
                                        cInt,
                                        cInt,
                                        cInt,
                                        cInt,
                                        ctypes.POINTER(ctypes.c_char))
        sensicamdll.SET_COC.restype = cInt
        self.errCheck(sensicamdll.SET_COC(self.hdriver,
                                          self.mode,
                                          self.trig,
                                          self.roixmin,
                                          self.roixmax,
                                          self.roiymin,
                                          self.roiymax,
                                          self.hbin,
                                          self.vbin,
                                          self.table))
        self.logger.debug("COC successful set")

    def testCoc(self, gain, camera_type, submode, roixmin, roixmax, roiymin,
                roiymax, hbin, vbin, table, trig=None):
        """The Camera Operation Code (COC) is tested with this function and the results returned.

        Further details can be obtained in the official SDK description.

        :param gain: chosen gain (0 normal analog, 1 extended analog)
        :type gain: int or ctypes.c_int
        :param camera_type: Selects the used camera (e.g. 5 for Dicam Pro)
        :type camera_type: int or ctypes.c_int
        :param submode: Choses the submode (e.g. 0)
        :type submode: int or ctypes.c_int
        :param roixmin: Region of interest min value in x
        :type roixmin: int or ctypes.c_int
        :param roixmax: Region of interest max value in x
        :type roixmax: int or ctypes.c_int
        :param roiymin: Region of interest min value in y
        :type roiymin: int or ctypes.c_int
        :param roiymax: Region of interest max value in y
        :type roiymax: int or ctypes.c_int
        :param hbin: Value for horizontal binning
        :type hbin: int or ctypes.c_int
        :param vbin: Value for vertical binning
        :type vbin: int or ctypes.c_int
        :param table: Set the delay and exposure times. Time values are
            separated by comma. The array concludes with the sequence
            "-1,-1". Detailed information in SDK manual
        :type table: str or ctypes.c_char_Array
        :param trig: Value for trigger. Default 0
        :type trig: int or ctypes.c_int
        """
        # Convert the input
        if int is type(camera_type):
            camera_type = cInt(camera_type)
        if int is type(gain):
            gain = cInt(gain)
        if int is type(submode):
            submode = cInt(submode)
        mode = cInt(camera_type.value +
                    (gain.value * 256) + (submode.value * 65536))  # p.24
        if trig is None:
            trig = cInt(0)
        elif int is type(trig):
            trig = cInt(trig)
        if int is type(roixmin):
            roixmin = cInt(roixmin)
        if int is type(roixmax):
            roixmax = cInt(roixmax)
        if int is type(roiymin):
            roiymin = cInt(roiymin)
        if int is type(roiymax):
            roiymax = cInt(roiymax)
        if int is type(hbin):
            hbin = cInt(hbin)
        if int is type(vbin):
            vbin = cInt(vbin)
        if str is type(table):
            ntable = ctypes.create_string_buffer(table.encode('utf-8'), 100)
        tableLen = cInt(ctypes.sizeof(ntable))
        nmode = copy.copy(mode)
        ntrig = copy.copy(trig)
        nroixmin = copy.copy(roixmin)
        nroixmax = copy.copy(roixmax)
        nroiymin = copy.copy(roiymin)
        nroiymax = copy.copy(roiymax)
        nhbin = copy.copy(hbin)
        nvbin = copy.copy(vbin)
        sensicamdll.TEST_COC.argtypes = (HANDLE,
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(ctypes.c_char),
                                         ctypes.POINTER(cInt))
        sensicamdll.TEST_COC.restype = cInt
        err = sensicamdll.TEST_COC(self.hdriver,
                                   nmode,
                                   ntrig,
                                   nroixmin,
                                   nroixmax,
                                   nroiymin,
                                   nroiymax,
                                   nhbin,
                                   nvbin,
                                   ntable,
                                   tableLen)
        if '0xc0003003' == (hex(DWORD(err).value - int('0x000a0000', 16))):
            warnings.warn('COC values have been altered (PCO_WARNING_SDKDLL_COC_VALCHANGE)')
            self.logger.warning('COC values have been altered (PCO_WARNING_SDKDLL_COC_VALCHANGE)')
            # Convert mode back
            submode = cInt(int(nmode.value / 65536))
            gain = cInt(int((nmode.value - submode.value * 65536) / 256))
            camera_type = cInt(nmode.value - gain.value * 256 - submode.value * 65536)
            return gain.value, camera_type.value, submode.value, nroixmin.value, nroixmax.value, nroiymin.value, \
                   nroiymax.value, nhbin.value, nvbin.value, ntable
        else:
            self.errCheck(err)
            self.logger.debug("COC successfully tested, nothing changed")
            return

    def runCoc(self, mode=None):
        """Starts the processing of the COC.

        :param mode: 0 for continuous, 4 for single trigger. Default 0
        :type: int or ctypes.c_int
        """
        if mode is None:
            mode = cInt(0)
        if int is type(mode):
            mode = cInt(mode)
        self.mode = mode
        sensicamdll.RUN_COC.argtypes = (HANDLE, cInt)
        sensicamdll.RUN_COC.restype = cInt
        self.errCheck(sensicamdll.RUN_COC(self.hdriver, mode))
        self.logger.debug("runCoc started")

    def stopCoc(self):
        """Stops the processing of the COC.

        Interrupt and active exposure (execution of the COC program). It can
        also be used as a break option, e.g. in the case of very long delay
        and exposure times.
        Additionally, the PCI interface Board Buffers are erased and the
        ability to transfer data is disabled.
        If the camera is running when thhis function is called, a waiting loop
        is started after stop to clear the CCD-Chip register contents.
        """
        sensicamdll.STOP_COC.argtypes = (HANDLE, cInt)
        sensicamdll.STOP_COC.restype = cInt
        self.errCheck(sensicamdll.STOP_COC(self.hdriver, self.mode))
        self.logger.debug("COC stopped")

    def getCocSetting(self):
        """Obtain the current COC settings from camera

        The values ccdxsize, ccdysize, curWidth, curHeight, curBitpix are read out from camera but not written to class
        object.
        """
        camera_type = cInt(-1)
        gain = cInt(-1)
        submode = cInt(-1)
        mode = cInt(camera_type.value +
                    (gain.value * 256) + (submode.value * 65536))  # p.24
        trig = cInt(0)  # Is set in table
        roixmin = cInt(-1)
        roixmax = cInt(-1)
        roiymin = cInt(-1)
        roiymax = cInt(-1)
        hbin = cInt(-1)
        vbin = cInt(-1)
        table = ctypes.create_string_buffer(200)
        tableLen = cInt(ctypes.sizeof(table))
        sensicamdll.GET_COC_SETTING.argtypes = (HANDLE,
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(ctypes.c_char),
                                                cInt)
        sensicamdll.GET_COC_SETTING.restype = cInt
        self.errCheck(sensicamdll.GET_COC_SETTING(self.hdriver,
                                                  mode,
                                                  trig,
                                                  roixmin,
                                                  roixmax,
                                                  roiymin,
                                                  roiymax,
                                                  hbin,
                                                  vbin,
                                                  table,
                                                  tableLen))
        # Convert mode back
        submode = cInt(int(mode.value / 65536))
        gain = cInt(int((mode.value - submode.value * 65536) / 256))
        camera_type = cInt(mode.value - gain.value * 256 - submode.value * 65536)
        self.logger.debug("COC setting aquired")
        return gain, camera_type, submode, roixmin, roixmax, roiymin, roiymax, hbin, vbin, table

    def getSizes(self):
        """Obtain information about the CCD and images sizes.

        The values ccdxsize, ccdysize, curWidth, curHeight, curBitpix are read out from camera and written to class
        object.
        """
        sensicamdll.GETSIZES.argtypes = (HANDLE,
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt),
                                         ctypes.POINTER(cInt))
        sensicamdll.GETSIZES.restype = cInt
        self.errCheck(sensicamdll.GETSIZES(self.hdriver,
                                           self.ccdxsize,
                                           self.ccdysize,
                                           self.curWidth,
                                           self.curHeight,
                                           self.curBitpix))
        self.logger.debug("ccd and image sizes aquired.")

    def getCamParam(self, camParam=None):
        """Obtain information about current cam parameters.

        :type camParam: class CamParam
        :return: camParam: class CamParam
        """
        if camParam is None:
            camParam = CamParam()
        sensicamdll.GET_CAM_PARAM.argtypes = (HANDLE, ctypes.POINTER(CamParam))
        sensicamdll.GET_CAM_PARAM.restype = cInt
        self.errCheck(sensicamdll.GET_CAM_PARAM(self.hdriver, camParam))
        self.logger.debug("camParam aquired.")
        return camParam

    def getCamValues(self, camVals=None):
        """Obtain information about current cam values.

        The function return camera values such as status (runcoc,..) and temperatures.

        :type camVals: class CamValues
        :return: camVals: class CamValues
        """
        if camVals is None:
            camVals = CamValues()
        sensicamdll.GET_CAM_VALUES.argtypes = (HANDLE, ctypes.POINTER(CamValues))
        sensicamdll.GET_CAM_VALUES.restype = cInt
        self.errCheck(sensicamdll.GET_CAM_VALUES(self.hdriver, camVals))
        self.logger.debug("camValues aquired")
        return camVals

    def allocateBuffer(self, curBuf):
        """Allocates a buffer for the camera in the memory.

         Set width, height and size of curBuf.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        curBuf.height = self.curHeight
        curBuf.width = self.curWidth
        curBuf.bitpix = self.curBitpix
        curBuf.sizeTh = cInt(curBuf.width.value * curBuf.height.value *
                             ctypes.sizeof(ctypes.c_ushort))
        curBuf.sizeReal = curBuf.sizeTh
        sensicamdll.ALLOCATE_BUFFER.argtypes = (HANDLE,
                                                ctypes.POINTER(cInt),
                                                ctypes.POINTER(cInt))
        sensicamdll.ALLOCATE_BUFFER.restype = cInt
        self.errCheck(sensicamdll.ALLOCATE_BUFFER(self.hdriver,
                                                  curBuf.bufnr,
                                                  curBuf.sizeReal
                                                  ))
        self.logger.debug("buffer allocated with number %d" % curBuf.bufnr.value)

    def mapBuffer(self, curBuf):
        """Maps the buffer and returns its address.

        The command maps the buffer and returns the address.
        If size is greater than allocated buffer size and error is returned.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.MAP_BUFFER.argtypes = (HANDLE, cInt, cInt, cInt,
                                           ctypes.POINTER(ctypes.POINTER(None)))
        sensicamdll.MAP_BUFFER.restype = cInt
        self.errCheck(sensicamdll.MAP_BUFFER(self.hdriver,
                                             curBuf.bufnr,
                                             curBuf.sizeTh,
                                             cInt(0),
                                             ctypes.pointer(curBuf.address)))
        self.logger.debug("buffer %d mapped" % curBuf.bufnr.value)

    def unmapBuffer(self, curBuf):
        """Unmaps the buffer.

        This command unmaps the buffer. Please unmap all mapped buffers
        before closing the driver.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.UNMAP_BUFFER.argtypes = (HANDLE, cInt)
        sensicamdll.UNMAP_BUFFER.restype = cInt
        self.errCheck(sensicamdll.UNMAP_BUFFER(self.hdriver,
                                               curBuf.bufnr))
        self.logger.debug("buffer %d unmapped" % curBuf.bufnr.value)

    def addBufferToList(self, curBuf):
        """Sets a buffer into the buffer queue. Should have been created with allocateBuffer.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.ADD_BUFFER_TO_LIST.argtypes = (HANDLE, cInt, cInt, cInt, cInt)
        sensicamdll.ADD_BUFFER_TO_LIST.restype = cInt
        self.errCheck(sensicamdll.ADD_BUFFER_TO_LIST(self.hdriver, curBuf.bufnr, curBuf.sizeReal, cInt(0), cInt(0)))
        self.logger.debug("buffer %d added to queue" % curBuf.bufnr.value)

    def removeBufferFromList(self, curBuf):
        """Removes the buffer curBuf from the buffer queue.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.REMOVE_BUFFER_FROM_LIST.argtypes = (HANDLE, cInt)
        sensicamdll.REMOVE_BUFFER_FROM_LIST.restype = cInt
        self.errCheck(sensicamdll.REMOVE_BUFFER_FROM_LIST(self.hdriver, curBuf.bufnr))
        self.logger.debug("buffer %d removed from queue" % curBuf.bufnr.value)

    def addBuffer(self, curBuf):
        """Adds buffer to the current list. The buffer is initialized.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        curBuf.rawImage = ctypes.create_string_buffer(curBuf.sizeTh.value)
        curBuf.address = ctypes.byref(curBuf.rawImage)
        sensicamdll.ADD_BUFFER.argtypes = (HANDLE,
                                           cInt,
                                           cInt,
                                           ctypes.c_void_p,
                                           HANDLE,
                                           ctypes.POINTER(DWORD))
        sensicamdll.ADD_BUFFER.restype = cInt
        self.errCheck(sensicamdll.ADD_BUFFER(self.hdriver,
                                             curBuf.bufnr,
                                             curBuf.sizeTh,
                                             curBuf.address,
                                             curBuf.hEvent,
                                             ctypes.byref(curBuf.status)
                                             ))
        self.logger.debug("buffer %d added" % curBuf.bufnr.value)

    def setBufferEvent(self, curBuf):
        """Create or attach an event handle for curBuf.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        :returns: curBuf.hEvent, event Handle for the buffer
        :rtype: ctypes.wintypes.HANDLE
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.SETBUFFER_EVENT.argtypes = (HANDLE,
                                                cInt,
                                                ctypes.POINTER(HANDLE))
        sensicamdll.SETBUFFER_EVENT.restype = cInt
        self.errCheck(sensicamdll.SETBUFFER_EVENT(self.hdriver,
                                                  curBuf.bufnr,
                                                  ctypes.byref(curBuf.hEvent)
                                                  ))
        self.logger.debug("event set for buffer %d" % curBuf.bufnr.value)

    def readImage12bit(self, curBuf):
        """Read an image in 12bit to the current selected buffer

        :Input:
        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        curBuf.bitpix.value = 12
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.READ_IMAGE_12BIT.argtypes = (HANDLE,
                                                 cInt,
                                                 cInt,
                                                 cInt,
                                                 ctypes.c_void_p)
        # use c_void_p instead of casting to c_ushort, because no casting necessary
        sensicamdll.READ_IMAGE_12BIT.restype = cInt
        self.errCheck(sensicamdll.READ_IMAGE_12BIT(self.hdriver,
                                                   cInt(0),
                                                   curBuf.width,
                                                   curBuf.height,
                                                   curBuf.address
                                                   ))
        self.logger.debug("Image aquired to buffer %d" % curBuf.bufnr.value)

    def waitForImage(self, timeout=1000):
        """Wait for the next to be aquired

        :param timeout: waiting time for new image in ms. Default 1000
        :type timeout: int
        """
        if int != type(timeout):
            raise TypeError('timeout must be int')
        sensicamdll.WAIT_FOR_IMAGE.argtypes = (HANDLE, cInt)
        sensicamdll.WAIT_FOR_IMAGE.restype = cInt
        self.errCheck(sensicamdll.WAIT_FOR_IMAGE(self.hdriver,
                                                 cInt(timeout)))

    def getImageFromBuffer(self, curBuf):
        """Get the image from an filled buffer.

        :Input:
        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        :returns: Acquired image (im and npImage)
        :rtype: numpy array (uint16)
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        if 12 == curBuf.bitpix.value:
            curBuf.rawImage = ((ctypes.c_ushort * int(curBuf.sizeReal.value / ctypes.sizeof(ctypes.c_ushort))).
                               from_address(curBuf.address.value))
            npimg = numpy.copy(numpy.frombuffer(curBuf.rawImage, dtype='uint16'))
            npimg = npimg.reshape((curBuf.height.value, curBuf.width.value))
            return npimg
        else:
            self.logger.error('unknown value for curBuf.bitpix')
            raise ValueError('unknown value for curBuf.bitpix')

    def removeAllBuffersFromList(self):
        """Removes all buffers from list.
        """
        sensicamdll.REMOVE_ALL_BUFFERS_FROM_LIST.argtype = HANDLE
        sensicamdll.REMOVE_ALL_BUFFERS_FROM_LIST.restype = cInt
        self.errCheck(sensicamdll.REMOVE_ALL_BUFFERS_FROM_LIST(self.hdriver))
        self.logger.debug("All buffers removed from list")

    def freeBuffer(self, curBuf):
        """Frees the given input buffer.

        :param curBuf: Buffer to which the image will be saved.
        :type curBuf: class pydicamSDK.ImgBuf
        """
        if ImgBuf != type(curBuf):
            raise TypeError('curBuf must be of class ImgBuf')
        sensicamdll.FREE_BUFFER.argtypes = (HANDLE, cInt)
        sensicamdll.FREE_BUFFER.restype = cInt
        self.errCheck(sensicamdll.FREE_BUFFER(self.hdriver,
                                              curBuf.bufnr
                                              ))
        self.logger.debug("buffer %d freed" % curBuf.bufnr.value)

    def setDicamWait(self, waitTime):
        """Sets waiting time for DICAM PRO.

        The camera must wait a specific t ime until the HV-module and pulser are ready for the next exposure.
        Waitinig time is done internally after programming the COC. `waitTime` cannot be smaller than 200ms.

        :param waitTime: time to wait in ms
        :type waitTime: int
        """
        if 200 > waitTime:
            raise ValueError('waitTime must be at least 200')
        sensicamdll.SET_DICAM_WAIT.argtype = (HANDLE,
                                              cInt)
        sensicamdll.SET_DICAM_WAIT.restype = cInt
        self.errCheck(sensicamdll.SET_DICAM_WAIT(self.hdriver, waitTime))
        self.logger.debug("dicam wait set to %d" % waitTime)

    def clearBoardBuffer(self):
        """A fast clear of the onboard buffers is done.

        Only one of the buffers is clear with each call to this function.
        If none of the buffers have valid data nothing is done.
        """
        sensicamdll.CLEAR_BOARD_BUFFER.argtype = HANDLE
        sensicamdll.CLEAR_BOARD_BUFFER.restype = cInt
        self.errCheck(sensicamdll.CLEAR_BOARD_BUFFER(self.hdriver))
        self.logger.info("Board buffer cleared")

    # noinspection PyIncorrectDocstring,PyIncorrectDocstring,PyMethodMayBeStatic
    def dicamTableGen(self, phosphordecay, mcpgain, trigger, loops,
                      imageTime1, *imageTimes):
        """Table generator for dicam Pro.

        Creates a valid table for the COC settings, which can be used in
        method setCoc.

        :param phosphordecay: Phosphor decay time in ms.
        :type phosphordecay: int
        :param mcpgain: MCP gain. Ranges from 0 to 999
        :type mcpgain: int
        :param trigger: Selects the trigger mode.
        :type trigger: int
        :param loops: Sets the number of loops.
        :type loops: int
        :param imagetime: list/tuple containing
                            delayhigh (ms), delaylow(ns),
                            timehigh(ms), timelow(ns)
        :type imagetime: list or tuple of int
        :returns: table for setCoc().
        :rtype: str
        """
        if type(imageTime1[0]) != int:
            self.logger.error('imageTime1 must be list of int')
            raise TypeError('imageTime1 must be list of int')
        if imageTimes:
            if type(imageTimes[0][0]) != int:
                self.logger.error('imageTimes must be list of int')
                raise TypeError('imageTimes must be list of int')
        table = (str(phosphordecay) + "," + str(mcpgain) + "," +
                 str(trigger) + "," + str(loops) + "," +
                 ','.join(str(tt) for tt in imageTime1))
        if 0 < len(imageTimes) < 4:
            for i in range(0, len(imageTimes)):
                table += ","
                table += ','.join(str(tt) for tt in imageTimes)
        # Set the end code
        table += ",-1,-1"
        return table

    def sensicamTableGen(self, submode, expos_width, delay=0):
        """Table generator for sensicam.

        Creates a valid table for the COC settings, which can be used in
        method setCoc.

        :param submode: submode of camera
        :type submode: int
        :param expos_width: exposure time. Check dimension in manual
        :type expos_width: int
        :param delay: delay for exposure. Default 0
        :type delay: int
        :return: table as string
        """
        if submode in [0, 1, 8, 20, 21]:
            table = "{},{},".format(delay, expos_width)
        elif submode == 9:
            table = ''
        else:
            self.logger.error("Submode not known/implemented.")
            raise ValueError("Submode not known/implemented.")
        # Set the end code
        table += "-1,-1"
        return table

    def __enter__(self):
        self.setup_camera()
        self.getSizes()
        return self

    # noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
    def __exit__(self, exc_type, exc_value, traceback):
        self.stopCoc()
        self.removeAllBuffersFromList()
        self.close()
