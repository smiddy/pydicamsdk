import ctypes
from ctypes.wintypes import DWORD, HANDLE
from win32event import WaitForSingleObject

# Small shortcut
cInt = ctypes.c_int


class ImgBuf():
    '''buffer class for buffer management in sensicam SDK.

    This class is used for the buffer management in class control. Each
    instance of class buffer can be used to get an image. No functions
    are implemented.

    :requires: Sensicam SDK (>=v6.01.04), pco.camware(>=v3.16)

    :param bufnr: number of buffer. Default -1 for allocation via SDK
    :type bufnr: ctypes.c_int
    :param size: buffersize in bits. Default 0
    :type size: ctypes.c_int
    :param bitpix: bits per pixel. Default 12
    :type bitpix: ctypes.c_int
    :param bufnr: number of buffer. Default -1
    :type bufnr: ctypes.c_int
    :param sizeTh: theoretical buffersize in bytes. Default 0
    :type sizeTh: ctypes.c_int
    :param sizeReal: real buffer size assigned by SDK. Default 0
    :type sizeReal: ctypes.c_int
    :param bitpix: bits per pixel. Default 12
    :type bitpix: ctypes.c_int
    :param width: pixels in x-direction of image. Default 0
    :type width: ctypes.c_int
    :param height: pixels in y-direction of image. Default 0
    :type height: ctypes.c_int
    :param address: address of buffer. Default None
    :type address: ctypes.c_void_p
    :param hEvent: Event handler. Default None
    :type hEvent: ctypes.wintypes.HANDLE
    :param status: status of buffer. Default -1
    :type status: ctypes.wintypes.DWORD
    :param rawImage: raw image. Default None
    :type rawImage: pillow image object

    Example::

        # Initiate an instance of buffer
        bufInst = imgBuf()
        # Create an array of buffers
        bufArray = []
        for i in range(10):
        bufArray.append(imgBuf())

    '''

    def __init__(self, bufnr=None, sizeTh=None, bitpix=None):
        '''
        Constructor
        '''
        if bufnr:
            self.bufnr = cInt(bufnr)
        else:
            self.bufnr = cInt(-1)
        if sizeTh:
            self.sizeTh = cInt(sizeTh)
        else:
            self.sizeTh = cInt(-1)
        if bitpix:
            self.bitpix = cInt(bitpix)
        else:
            self.bitpix = cInt(-1)
        self.sizeReal = None
        self.width = -1
        self.height = -1
        self.address = ctypes.c_void_p(None)
        self.hEvent = HANDLE(None)
        self.status = DWORD(999)
        self.rawImage = None

    def getStatus(self):
        ''' Get current buffer status with WaitForSingleObject

        :returns: status
        :rtype: int
        '''
        status = WaitForSingleObject(self.hEvent.value, 0)
        return status


if __name__ == '__main__':
    # Initiate an instance of buffer
    bufInst = ImgBuf()
    print(bufInst.bufnr)
    del bufInst
    # Create an array of buffers
    ii = 10
    bufArray = [ImgBuf() for ii in range(10)]
    for ii in range(10):
        print(bufArray[ii].bufnr)
