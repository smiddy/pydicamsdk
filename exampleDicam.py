import dicamsdk
import logging
import matplotlib.pyplot as plt

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
board = 0
with dicamsdk.Sensicam(board) as pcoCam:
    # Setup a the COC
    gain = 0  # normal analog gain
    submode = 0  # sequential, busy out
    roixmin = 1
    roixmax = 1
    roiymin = 1
    roiymax = 1
    hbin = 1
    vbin = 1
    # Table values:
    phosphordecay, mcpgain, trigger, loops = 1, 999, 0, 1
    imageTime1 = [0, 1000, 0, 300]
    table = pcoCam.dicamTableGen(phosphordecay, mcpgain, trigger, loops,
                                 imageTime1)
    camera_type = 5  # For dicam pro
    pcoCam.setCoc(gain, camera_type, submode, roixmin, roixmax, roiymin,
                  roiymax, hbin, vbin, table)
    print("COC loaded")
    # Get sizes and allocate buffer
    pcoCam.getSizes()
    # Initialize buffers
    buf1 = dicamsdk.ImgBuf()
    buf2 = dicamsdk.ImgBuf()
    pcoCam.allocateBuffer(buf1)
    pcoCam.mapBuffer(buf1)
    pcoCam.setBufferEvent(buf1)
    pcoCam.allocateBuffer(buf2)
    pcoCam.mapBuffer(buf2)
    pcoCam.setBufferEvent(buf2)
    # Clear the board buffer with stopCoc command
    pcoCam.stopCoc()
    print('status buf1:', buf1.getStatus())
    # Set camera to state RUN
    pcoCam.runCoc()
    pcoCam.addBufferToList(buf1)
    pcoCam.addBufferToList(buf2)
    print('status buf1:', buf1.getStatus())
    pcoCam.stopCoc()
    npImage1 = pcoCam.getImageFromBuffer(buf1)
    npImage2 = pcoCam.getImageFromBuffer(buf2)
    # Free all buffers and close the camera
    pcoCam.removeAllBuffersFromList()
    pcoCam.freeBuffer(buf1)
    pcoCam.freeBuffer(buf2)
    # Show the image
    plt.ion()
    plt.imshow(npImage1)
print("Card closed.")
