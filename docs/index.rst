.. pydicamsdk documentation master file, created by
   sphinx-quickstart on Wed Sep  2 14:43:50 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pydicamsdk's documentation!
======================================

A wrapper for the pco sensicam/dicam SDK

This class is a Python wrapper for the Sensicam SDK of pco AG, Germany and provides an API to handle an pco camera in python. It is tested and used with a Dicam Pro.

.. toctree::
   :maxdepth: 2

   sensicam
   cstructures
   imgBuf

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Information
===========
:version: 0.2.0
:license: GNU GPLv3
:author: Markus J Schmidt
:organization: Institute of Fluid Dynamics, ETH Zurich, Switzerland
