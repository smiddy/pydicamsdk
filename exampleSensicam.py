import dicamsdk
import logging
import matplotlib.pyplot as plt
from time import sleep

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

board = 1

with dicamsdk.Sensicam(board) as pcoCam:
    # Setup a the COC
    gain = 0  # normal analog gain
    submode = 0  # sequential, busy out
    roixmin = 1
    roixmax = 10
    roiymin = 1
    roiymax = 10
    hbin = 1
    vbin = 1
    # Table values:
    trigger = int(0x000)
    exposure = 100
    delay = 222
    camera_type = 0  # For dicam pro
    table = pcoCam.sensicamTableGen(submode, exposure, delay)
    pcoCam.setCoc(gain, camera_type, submode, roixmin, roixmax, roiymin,
                  roiymax, hbin, vbin, table, trigger)
    print("COC loaded")
    # Get sizes and allocate buffer
    pcoCam.getSizes()
    # Initialize buffers
    buf1 = dicamsdk.ImgBuf()
    buf2 = dicamsdk.ImgBuf()
    pcoCam.allocateBuffer(buf1)
    pcoCam.mapBuffer(buf1)
    pcoCam.setBufferEvent(buf1)
    pcoCam.allocateBuffer(buf2)
    pcoCam.mapBuffer(buf2)
    pcoCam.setBufferEvent(buf2)
    # Clear the board buffer with stopCoc command
    pcoCam.stopCoc()
    print('status buf1:', buf1.getStatus())
    b2status = buf2.getStatus()
    # Set camera to state RUN
    pcoCam.addBufferToList(buf1)
    pcoCam.addBufferToList(buf2)
    pcoCam.runCoc()
    while buf2.getStatus() == b2status:
        sleep(0.1)
    print('status buf1:', buf1.getStatus())
    pcoCam.stopCoc()
    npImage1 = pcoCam.getImageFromBuffer(buf1)
    npImage2 = pcoCam.getImageFromBuffer(buf2)
    # Free all buffers and close the camera
    pcoCam.removeAllBuffersFromList()
    pcoCam.freeBuffer(buf1)
    pcoCam.freeBuffer(buf2)
    # Show the image
    plt.ion()
    plt.imshow(npImage1)
print("Card closed.")
